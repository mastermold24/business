"use strict";
//  node modules
var Hapi = require('hapi');
var Path = require('path');
//  sertup constants
var constants = require('./src/config/constants.js');
var basicAuth = require('./src/middleware/basic-auth');
var plugins   = require('./src/plugins/index');
var routes    = require('./src/routes');
//  new hapi server
var server = new Hapi.Server();
var connectionSpec = {
    host : 'localhost' || process.env.IP,
    port : 5000 || process.env.PORT,
}
//config server
server.connection(connectionSpec);
server.state('data', {
    ttl: null,
    isSecure: true,
    isHttpOnly: true,
    encoding: 'base64json',
    clearInvalid: false, // remove invalid cookies
    strictHeader: true   // don't allow violations of RFC 6265
});
// add plugins
for (var p in plugins) {
	plugins[p](server);
}
// add routes  
for (var route in routes) {
	server.route(routes[route]);
}
// handle init
module.exports = server;
if (process.env.NODE_ENV !== 'test') {
	server.start();
	console.log(`Server running port: ${constants.application['port']}`);
}
