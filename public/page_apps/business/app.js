// declare a module
var businessApp = angular.module('businessApp', ['ui.router','ui.bootstrap','ngAnimate']);
// handle configuration [routing]
businessApp.config(['$stateProvider','$urlRouterProvider', function($stateProvider,$urlRouterProvider) {
  // for any unmatched url, redirect to /state1
  $urlRouterProvider.otherwise("/home");
  // now set up the states
  $stateProvider
    .state('home', {
      url: "/home",
      controller: 'homeController',
      templateUrl: appendDirRoot("partials/home.html"),
    })
    .state('demo', {
      url: "/demo",
      templateUrl: appendDirRoot("partials/demo.html")
    })
    .state('pricing', {
      url: "/pricing",
      templateUrl: appendDirRoot("partials/pricing.html")
    })
    .state('test', {
      url: "/test",
      templateUrl: appendDirRoot("partials/test.html"),
      controller: function($scope) {
        $scope.items = ["A", "List", "Of", "Items"];
      }
    })
    .state('state2', {
      url: "/state2",
      templateUrl: "partials/demo.html"
    })
    .state('state2.list', {
      url: "/list",
      templateUrl: "partials/state2.list.html",
      controller: function($scope) {
        $scope.things = ["A", "Set", "Of", "Things"];
      }
    });

    function appendDirRoot(a) {
      var ROOT_APP_DIRECTORY = "../page_apps/business/";
      return ROOT_APP_DIRECTORY + a ;
    }

}]).directive("scrollTopOnRouteChange", [
    "$rootScope",
    function ($rootScope) {
        return {
            restrict: "A",
            link : function ($scope, $element) {
                $rootScope.$on("$routeChangeSuccess", function () {
                    $element.scrollTop(0);
                });
            }
        };
    }
]);
