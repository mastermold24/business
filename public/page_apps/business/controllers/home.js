businessApp.controller('homeController', ['$scope','$uibModal', '$log', function($scope, $uibModal, $log) {
  // controller bootstrap
  $scope.init = function (){
    // init jquery
    $('#qPhone').mask('(000)-000-0000');
    $('.mask-phone').mask('(000)-000-0000');
    // define forms models
    $scope.leadBoxModel = {phone_number:'613'};
    // products and services
    $scope.offersProductsServices = [1,2];
    // featured demos
    $scope.featuredDemos = [1,2];
  }
  // modal handlers
  $scope.viewDemoInModal = function () {
    var modalInstance = $uibModal.open({
      animation: true,
      templateUrl: 'myModalContent.html',
      controller:  'ScanModalController',
      size: 'lg'
    });
    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
  // toggle animation
  $scope.toggleAnimation = function () {
    $scope.animationsEnabled = !$scope.animationsEnabled;
  };
  // setup ctrl
  $scope.init();
}]);
// modal controller
businessApp.controller('ScanModalController', function ($scope, $uibModalInstance) {
  // controller bootstrap
  $scope.init = function(){
    $scope.alerts = [
      { type: 'danger', msg: 'Oh snap! Change a few things up and try submitting again.' },
      { type: 'success', msg: 'Well done! You successfully read this important alert message.' }
    ];
  }
  // alert functions
  $scope.addAlert = function() {
    $scope.alerts.push({msg: 'Another alert!'});
  };
  $scope.closeAlert = function(index) {
   $scope.alerts.splice(index, 1);
  };
  // modal functions
  $scope.ok = function () {
    $uibModalInstance.close($scope.selected.item);
  };
  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
  // setup ctrl
  $scope.init();
});
