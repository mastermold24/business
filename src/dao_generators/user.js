"use strict";
//  NPM MODULES
var merge = require('tea-merge');
//  DEFINE GEN
function UserGen() {};
UserGen.prototype = (function() {
    return {
        // @return <Object>
        genInitUser: (inputObj) => {
            return merge(genInitDefaults(),inputObj)
        }
    };
})();
module.exports = new UserGen();
function genInitDefaults (){
  return {
      "proffesion": "realtor",
      "addresses": null,
      "personal_first_name": "UNDEFINED",
      "auth_password": "UNDEFINED",
      "personal_last_name": "UNDEFINED",
      "auth_email": "UNDEFINED",
      "is_tfa_enabled": false,
      "date_registered": null,
      "is_admin": false,
      "is_deleted": false,
      "user_id": "UNDEFINED",
      "is_banned": false,
  }
}
