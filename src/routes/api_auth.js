"use strict";
//  NPM MODULES
var authController = require('../controllers/auth');
var authValidate   = require('../validate/auth');
//  DEFINE ROUTES
module.exports = function() {
	return [
    { // handles account authentication
  		method: 'POST',
  		path:   '/api/login',
      config : {
				handler  : authController.login,
				validate : authValidate.login
			}
    },
    { // handles account registry
      method: 'POST',
      path:   '/api/auth/register',
      config : {
        handler  : authController.login,
        validate : authValidate.register
      }
    },
		{ // handles account reset
			method: 'POST',
			path:   '/api/auth/reset_password/{reset_id}',
			config : {
				handler  : authController.login,
				validate : authValidate.reset_password
			}
		}
	];
}();
