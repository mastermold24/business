"use strict";
//  NPM MODULES
var fileController = require('../controllers/file');
var fileValidate   = require('../validate/file');
//  DEFINE ROUTES
module.exports = function() {
	return [
		{ // insert file
		  method: 'POST',
			path:   '/api/files',
		  config: {
				payload: {
	            output: 'stream',
	            parse: true,
	            allow: 'multipart/form-data'
	      },
		    handler: fileController.insert
		  }
		},
		{ // retrieve file by id
			method: 'GET',
			path:   '/api/files/{file_id}',
			config: {
				handler:  fileController.findByID,
      	validate: fileValidate.refId
			}
		},
		{ // updates file
			method: 'PUT',
			path:   '/api/filess/{file_id}',
			config: {
				handler:  fileController.update,
  			validate: fileValidate.refId
			}
		},
		{ // deletes file
			method: 'DELETE',
			path:   '/api/files/{file_id}',
			config: {
				handler:  fileController.delete,
  			validate: fileValidate.refId
			}
		}
	];
}();
