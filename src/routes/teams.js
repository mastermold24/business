"use strict";
//  npm modules
var teamController = require('../controllers/user');
var teamValidate   = require('../validate/user');
//  define routes
module.exports = function() {
	return [
		{ // creates new team
			method: 'POST',
			path: '/api/teams',
			config : {
				handler : teamController.insert,
				//validate : teamValidate.insert
			}
		},
		{ // get user's teams
			method: 'GET',
			path: '/api/teams',
			config : {
				handler: teamController.find,
				//validate : teamValidate.find
			}
		},
		{ // get specific team
			method: 'GET',
			path: '/api/teams/{team_id}',
			config : {
				handler: teamController.findByID,
				//validate: teamValidate.findByID
			}
		},
		{ // updates a team
			method: 'PUT',
			path: '/api/teams/{team_id}',
			config : {
				handler: teamController.update,
				//validate : teamValidate.update
			}
		},
    { // updates a team
      method: 'PUT',
      path: '/api/teams/{team_id}/suspend',
      config : {
        handler: teamController.update,
        //validate : teamValidate.update
      }
    },
		{ // marks team as deleted
			method: 'DELETE',
			path: '/api/teams/{team_id}/delete',
			config : {
				handler: teamController.delete,
				//validate : teamValidate.delete
			}
		},
		{ // removes team from db
			method: 'DELETE',
			path: '/api/teams/{team_id}/remove',
			config : {
				handler: teamController.delete,
				//validate : teamValidate.delete
			}
		}
	];
}();
