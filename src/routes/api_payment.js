"use strict";
//  npm modules
var paymentController = require('../controllers/payment');
var paymentValidate   = require('../validate/payment');
//  define routes
module.exports = function() {
	return [
		{ // creates new payment
			method: 'POST',
			path: '/api/payments',
			config : {
				handler:  paymentController.insert,
				validate: paymentValidate.insert
			}
		},
		{ // get user's payments
			method: 'GET',
			path: '/api/payments',
			config : {
				handler: paymentController.find,
				//validate : paymentValidate.find
			}
		},
		{ // get payments by companies
			method: 'GET',
			path: '/api/payments/by_companies',
			config : {
				handler: paymentController.findByID,
				//validate: paymentValidate.refByID
			}
		},
		{ // get payments by a company
			method: 'GET',
			path: '/api/payments/by_companies/{company_id}',
			config : {
				handler: paymentController.findByID,
				validate: paymentValidate.refByID
			}
		},
		{ // get payments by teams
			method: 'GET',
			path: '/api/payments/by_teams',
			config : {
				handler:  paymentController.findByID,
				//validate: paymentValidate.refByID
			}
		},
		{ // get payments by a team
			method: 'GET',
			path: '/api/payments/by_teams/{team_id}',
			config : {
				handler:  paymentController.findByID,
				//validate: paymentValidate.refByID
			}
		},
		{ // get specific payment
			method: 'GET',
			path: '/api/payments/{payment_id}',
			config : {
				handler:  paymentController.findByID,
				validate: paymentValidate.refByID
			}
		},
		{ // updates a payment
			method: 'PUT',
			path: '/api/payments/{payment_id}',
			config : {
				handler:  paymentController.update,
				validate: paymentValidate.update
			}
		},
    { // updates a payment
      method: 'PUT',
      path: '/api/payments/{payment_id}/cancel',
      config : {
        handler:  paymentController.update,
        validate: paymentValidate.refByID
      }
    },
		{ // marks payment as deleted
			method: 'DELETE',
			path: '/api/payments/{payment_id}/delete',
			config : {
				handler:  paymentController.delete,
				validate: paymentValidate.refByID
			}
		},
		{ // removes payment from db
			method: 'DELETE',
			path: '/api/payments/{payment_id}/remove',
			config : {
				handler:  paymentController.delete,
				validate: paymentValidate.refByID
			}
		}
	];
}();
