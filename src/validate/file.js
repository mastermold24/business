"use strict";
//  NPM MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SCHEMAS
function FileValidate(){};
FileValidate.prototype = (function(){
	return {
		find : {
			query: (function query() {
				var fileSchema = new models.File().schema;
				return {
					file_id: fileSchema.file_id
				};
			})()
		},
		refByID: {
			params: (function params() {
				var fileSchema = new models.File().schema;
				return {
					file_id: fileSchema.file_id.required(),
				};
			})()
		},
		insert: {
			payload: (function payload() {
				var fileSchema = new models.File().schema;
				return {
 					file_id: fileSchema.file_id.required()
				};
			})()
		},
		update: (function update() {
			var fileSchema = new models.File().schema;
			return {
				params: {
					file_id: fileSchema.file_id.required()
				},
				payload: {

				}
			}
		})(),
		delete: {
			params: (function params() {
				var fileSchema = new models.File().schema;
				return {
					file_id: fileSchema.file_id.required()
				};
			})()
		},
		remove: {
			params: (function params() {
				var fileSchema = new models.File().schema;
				return {
					file_id: fileSchema.file_id.required()
				};
			})()
		}
	};
})();
module.exports = new FileValidate();
