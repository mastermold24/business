"use strict";
//  NODE MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SPEC
function UserValidate(){};
UserValidate.prototype = (function(){
	return {
    // authenticate client
		login : {
			payload: (function query() {
				var userSchema = new models.User().schema;
				return {
					auth_username: userSchema.auth_username,
          auth_password: userSchema.auth_password
				};
			})()
		},
    // create user account
    register : {
			payload: (function query() {
				var userSchema = new models.User().schema;
				return {
					auth_username: userSchema.auth_username,
          auth_password: userSchema.auth_password,
          personal_first_name: userSchema.personal_first_name,
      		personal_last_name:  userSchema.personal_last_name
				};
			})()
		},
    // reset user account
    reset_password : {
			payload: (function query() {
				var userSchema = new models.User().schema;
				return {
					auth_username: userSchema.auth_username,
          auth_password: userSchema.auth_password,
				};
			})()
		}
	};
})();
module.exports = new UserValidate();
