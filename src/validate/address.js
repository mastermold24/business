"use strict";
//  NPM MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SCHEMAS
function AddressValidate(){};
AddressValidate.prototype = (function(){
	return {
		find : {
			query: (function query() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.address_id
				};
			})()
		},
		findByID: {
			params: (function params() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.address_id.required(),
				};
			})()
		},
		refByID: {
			params: (function params() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.address_id.required(),
				};
			})()
		},
		findByIdColleagues: (function update() {
			var addressSchema = new models.Address().schema;
			return {
				params: {
					address_id: addressSchema.address_id.required()
				},
				query: {
					 limit: Joi.number().integer().min(1).max(100).default(10)
				}
			}
		})(),
		findByIdTeamMates: {
			params: (function params() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.auth_addressname.required(),
				};
			})()
		},
		insert: {
			payload: (function payload() {
				var addressSchema = new models.Address().schema;
				return {
					auth_email: addressSchema.auth_email.required(),
					auth_addressname: addressSchema.auth_addressname.required(),
					auth_password: addressSchema.auth_password.required()
				};
			})()
		},
		update: (function update() {
			var addressSchema = new models.Address().schema;
			return {
				params: {
					address_id: addressSchema.address_id.required()
				},
				payload: {
					auth_email: addressSchema.auth_email,
					auth_addressname: addressSchema.auth_addressname,
					auth_password: addressSchema.auth_password
				}
			}
		})(),
		delete: {
			params: (function params() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.address_id.required()
				};
			})()
		},
		remove: {
			params: (function params() {
				var addressSchema = new models.Address().schema;
				return {
					address_id: addressSchema.address_id.required()
				};
			})()
		}
	};
})();
module.exports = new AddressValidate();
