"use strict";
//  NPM MODULES
var _ = require('underscore');
var Joi = require('joi');
//  PACKAGES
var models = require('../models');
//  VALIDATION SCHEMAS
function PaymentValidate(){};
PaymentValidate.prototype = (function(){
	return {
		find : {
			query: (function query() {
				var paymentSchema = new models.Payment().schema;
				return {
					payment_id: paymentSchema.payment_id
				};
			})()
		},
		refByID: {
			params: (function params() {
				var paymentSchema = new models.Payment().schema;
				return {
					payment_id: paymentSchema.payment_id.required(),
				};
			})()
		},
		insert: {
			payload: (function payload() {
				var paymentSchema = new models.Payment().schema;
				return {
 					payment_id: paymentSchema.payment_id.required()
				};
			})()
		},
		update: (function update() {
			var paymentSchema = new models.Payment().schema;
			return {
				params: {
					payment_id: paymentSchema.payment_id.required()
				},
				payload: {

				}
			}
		})(),
		delete: {
			params: (function params() {
				var paymentSchema = new models.Payment().schema;
				return {
					payment_id: paymentSchema.payment_id.required()
				};
			})()
		},
		remove: {
			params: (function params() {
				var paymentSchema = new models.Payment().schema;
				return {
					payment_id: paymentSchema.payment_id.required()
				};
			})()
		}
	};
})();
module.exports = new PaymentValidate();
