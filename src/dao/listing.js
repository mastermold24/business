"use strict";
//  import database
var db  = require('../middleware/db');
var oDB = db.orientDB
var util = require('./_helpers');
//  import packages
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
//  define dao
function ListingDAO() {};
//  define methods
ListingDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  helper values
    var classEnum = util.classes;
    //  define the dao
    return {
        // @return <Object>
        insertListing: function (params, data) {
            var sql = util.sls `select expand(registerListing())`;
            return oDB.insert().into(classEnum.Listing).set(data).one();
        },
        // @return <Array<Object>
        findListingByID: function (params, meta_id) {
          var where = {meta_id: meta_id};
          return oDB.select('*').from(classEnum.Listing).where(where).all()
        },
        // @return
        findListingByCompany: function (params,stipulations) {
          return oDB.select('*').from(classEnum.Listing).where(stipulations).all()
        },
        // @return
        findListingByTeam: function (params,stipulations) {
          return oDB.select('*').from(classEnum.Listing).where(stipulations).all()
        },
        // @return
        createListing: function (listing_id,data) {
          var sql = util.sls `select expand(registerListing())`;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return
        updateListing: function (listing_id, data) {
          var where = {meta_id: data.meta_id};
          return oDB.update(classEnum.Listing).set(data).where(listing_id: listing_id }).scalar();
        },
        expireListing: function (listing_id) {
          var where = {meta_id: data.meta_id};
          return oDB.update(classEnum.Listing).set({is_expired: false}).where(listing_id: listing_id }).scalar();
        },
        // @return
        deleteListing: function(listing_id) {
            var where = filter;
            return oDB.update(classEnum.Listing).set({is_deleted: true}).where(where).scalar();
        },
        // @return
        removeListing: function(listing_id) {
            var where = filter;
            return oDB.delete().from(classEnum.Listing).where({listing_id: listing_id }).limit(1).scalar()
        },
    };
})();
module.exports = new ListingDAO();
