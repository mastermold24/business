"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
//  PACKAGES
var db   = require('../middleware/db');
var oDB  = db.orientDB
var util = require('./_helpers');
var companyGen = require('../dao_generators/company');
//  DAO CONSTRUCTOR
function CompanyDAO() {};
CompanyDAO.prototype = (function() {
    //  default values
    var MAX_RECORD_QUANTITY = 150;
    //  helper values
    var classEnum = util.classes;
    //  dao methods
     return {
        // @return Array[Object]
        insertCompany: function (data) {
          var companyDataJSON = JSON.stringify(companyGen.genInitCompany(data));
          var sql = util.sls `select expand(registerCompany(${companyDataJSON}))`;
          return oDB.insert().into(classEnum.Company).set(data).one();
        },
        // @return Array[Object]
        matchCompany: function (criteriaRecord) {
          return oDB.select('*').from(classEnum.Company).where(criteriaRecord).all()
        },
        // @return Array[Object]
        findByIDCompany: function (company_id,isDeleted,isCancelled) {
          return oDB.select('*').from(classEnum.Company).where({
            company_id:  company_id,
            is_deleted: isDeleted || false,
            is_cancelled: isCancelled || false,
          }).all()
        },
        // @return Array[Object]
        countAppCompanys: function (filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.Company).where(where).all()
        },
        // @return Array[Object]
        countCompanysByUser: function (user_id) {
          return oDB.select('count(*)').from(classEnum.Company).where({user_id:user_id}).all()
        },
        // @return Array[Object]
        createCompany: function (company_id,data) {
          var sql = util.sls `select expand(registerCompany())`;
          return oDB.query(sql,{ params: {}, limit: (params.limit || MAX_RECORD_QUANTITY) });
        },
        // @return Number
        updateCompany: function (company_id, data) {
          var where = {company_id: data.company_id};
          return oDB.update(classEnum.Company).set(data).where({company_id: company_id}).scalar();
        },
        // @return Number
        unCancelCompany: function(company_id) {
            return oDB.update(classEnum.Company).set({is_cancelled: false}).where({company_id: company_id}).scalar();
        },
        // @return Number
        cancelCompany: function(company_id) {
          var where = filter;
          return oDB.update(classEnum.Company).set({is_cancelled: true}).where({company_id: company_id}).scalar();
        },
        // @return Number
        deleteCompany: function(company_id) {
          return oDB.update(classEnum.Company).set({is_deleted:true}).where({company_id: company_id}).scalar();
        },
        // @return Number
        removeCompany: function(company_id) {
          return oDB.delete().from(classEnum.Company).where({company_id: company_id }).limit(1).scalar()
        }
    };
})();
module.exports = new CompanyDAO();
