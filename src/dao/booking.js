"use strict";
//  NPM MODULES
var S = require('string');
var SqlGenerator = require('sql-generator');
var sqlgen = new SqlGenerator();
//  PACKAGES
var db  = require('../middleware/db');
var oDB = db.orientDB
var helpers = require('./_helpers');
var classEnum = helpers.classes;
//  DEFINE DAO
function BookingDAO() {};
BookingDAO.prototype = (function() {
    return {
        // @return <Array<Object>
        insertBooking: function (params,data) {
          return oDB.insert().into(classEnum.Booking).set(data).one();
        },
        // @return <Array<Object>
        matchBookings: function (rec) {
          return oDB.select('*').from(classEnum.Booking).where(rec).all();
        },
        // @return <Array<Object>
        findByIDBooking: function (booking_id) {
          return oDB.select('*').from(classEnum.Booking)
          .where({ booking_id: booking_id, is_deleted: false}).all();
        },
        // @return <Number>
        countBookings: function (params,filter) {
          var where = filter || {};
          return oDB.select('count(*)').from(classEnum.Booking).where(where).all();
        },
        // @return <Array<Object>
        updateBooking: function (booking_id,data) {
          return oDB.update(classEnum.Booking).set(data).where({booking_id: booking_id}).scalar();
        },
        // @return Number
        unCancelBooking: function (booking_id) {
          return oDB.update(classEnum.Booking).set({is_cancelled:false}).where({booking_id: booking_id}).scalar();
        },
        // @return Number
        cancelBooking: function (booking_id) {
          return oDB.update(classEnum.Booking).set({is_cancelled:true}).where({booking_id: booking_id}).scalar();
        },
        // @return Number
        deleteBooking: function(booking_id) {
            var where = filter;
            return oDB.update(classEnum.Booking).set({is_deleted: true}).where(where).scalar();
        },
        // @return Number
        removeBooking: function(params) {
            var where = filter;
            return oDB.delete().from(classEnum.Booking).where({booking_id: booking_id }).limit(1).scalar()
        }
    };
})();
module.exports = new BookingDAO();
