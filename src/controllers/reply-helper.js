"use strict";
//  NPM MODULES
var _ = require('underscore');
var Boom = require('boom');
var Hapi = require('hapi');
//  Modules
var constants = require('../config/constants');
var paginationLinks = require('../util/pagination-links');
var li = require('li');
// DEFINE HELPER
function ReplyHelper(request, reply) {
	this.request = request;
	this.reply   = reply;
	this.url = request.headers.host ? 'http://' + request.headers.host : constants.server.defaultHost;
}
// handle one item
ReplyHelper.prototype.replyFindOne = function replyFindOne(data) {
	if (_.isArray(data) && data.length === 1) {
		return this.reply({ data: data[0], success: true});
	}
	if (_.isArray(data) && data.length === 0) {
			return this.reply(Boom.create(400, 'Bad request', { timestamp: Date.now() }));
	}
	return this.reply({ data: data, success: true});
};
/*
// handle many
ReplyHelperhapi-auth-cookie-jwt.prototype.replyFindMany = function replyFindOne(dataz) {
    var respObj = { data: dataz };
		return this.reply(respObj);
};*/
// handle error
ReplyHelper.prototype.replyError = function replyFindOne(err) {
	console.log(this.reply)
		return this.reply(Boom.badImplementation('terrible implementation'));
};
// handle pagination
ReplyHelper.prototype.replyFind = function replyFind(err, data) {
	if (err) return this.reply(Hapi.error.badImplementation(err));
	var linksHeader = paginationLinks.create({
		url : this.url + this.request.path,
		page : this.request.query.page,
		perPage : this.request.query.perPage,
		totalCount : data.length
	});
	var response = this.reply(data).hold();
	if (!_.isEmpty(linksHeader)) {
		response.header('Link', li.stringify(linksHeader));
	}
	response.type('application/json')
		.header('Total-Count', data.length)
		.send();
};
//
ReplyHelper.prototype.replyDelete = function replyDelete(err, data) {
	if (err) return this.reply(Hapi.error.badImplementation(err));
	this.reply().code(204);
};

module.exports = ReplyHelper;
