"use strict";
//  NPM MODULES
var _ = require('underscore');
var Boom = require('boom');
//  PACKAGES
var userDAO = require('../dao/user');
var constants = require('../config/constants.js');
var tokenGenerator = tokenGenFactory(constants.application.secret)
var helpers = require('./_helpers');
var headerEnum  = helpers.headerEnum;
var ReplyHelper = require('../controllers/reply-helper');
//  LOCAL HELPERS
function tokenGenFactory(secret) {
	return (data) => jwt.sign(data, secret);
}
//  DEFINE CONTROLLER
function userController() {};
userController.prototype = (function() {
	var auth_codes = {
		DATABASE_DOWN_ERROR: 'DATABASE_DOWN_ERROR',
		MISC_ERROR:	'MISC_ERROR'
	}
	return {
		register: function findByID(request, reply) {
			var helper   = new ReplyHelper(request, reply);
			var userData = request.payload;
			console.log(userData)
			userDAO.findSimpleUsers({
				auth_username: 'nicko',
				auth_password: '12345'
			}).then(handleSuccess).catch(handleFailure);

			function handleSuccess(data) {
				if (typeof data[0] !== 'undefined') {
					return reply(data[0]).type(headerEnum.json).code(201);
				} else {
					return reply(Boom.create(400, 'BAD_CREDENTIALS'));
				}
			}

			function handleFailure(err) {
				return reply(Boom.wrap(err, 400));
			}
		},
		login: function find(request, reply) {
			var helper = new ReplyHelper(request, reply);
			userDAO.matchUsers(request.payload).then(handleSuccess).catch(handleFailure);
			function handleSuccess(data) {
				if (typeof data[0] !== 'undefined') {
						console.log(tokenGenerator(data))
					return reply(data[0]).state('access_token', tokenGenerator(data)).code(201);
				} else {
						console.log(2)
					return reply.redirect(`/login?is_error=true&error_code=${auth_codes.MISC_ERROR}`);
				}
			}
			function handleFailure(err) {
					return  reply.redirect('/login?is_error=true&error_code=misc');
			}
		},
		logout: function findByID(request, reply) {
			var helper = new ReplyHelper(request, reply);
		}
	}
})();
module.exports = new userController();
