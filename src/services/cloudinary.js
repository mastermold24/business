"use strict";
//  PACKAGES
var cloudinarySVC = require('../middleware/services');
var cloudinary = cloudinarySVC.cloudinary;
//  DEFINE SVC
function FileSVC() {};
FileSVC.prototype = (function() {
    return {
        insertImage: function (fileToUpload) {
          console.log('uploading...')
            return cloudinary.uploader.upload(fileToUpload, function(result) {
              console.log(result)
            });
        },
        insertFile: function (fileToUpload) {
            return cloudinary.uploader.upload(fileToUpload, function(result) {
              console.log(result)
            });
        }
    };
})();
module.exports = new FileSVC();
