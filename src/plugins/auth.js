"use strict";
//  NPM MODULES
var jwt = require('jsonwebtoken');
var Joi = require('joi');
//  PACKAGES
var constants  = require('../config/constants.js');
var privateKey = constants.application.secret;
// register authentications
module.exports = function (server) {
 // validate strategy
 var validateTokenAsync = function (decodedToken, callback) {
     var error = null;
     var credentials = accounts[decodedToken.accountId] || {};
     if (!credentials) {
         return callback(error, false, credentials);
     }
     return callback(error, true, credentials)
 };/*
 // register authentication module
 server.register(require('hapi-auth-cookie-jwt'), function (error) {
     server.auth.strategy('token', 'jwt-cookie', {
         key: privateKey,
         validateFunc: validateTokenAsync
     });
 });*/
}
//
function validateJWTSchema () {
    var schema = {
  		user_id: Joi.number().integer().required(),
  		email:  Joi.string().required()
  	};
    var err = Joi.validate(credentials, schema, {allowUnknown:true});
  	return err === null;
}
